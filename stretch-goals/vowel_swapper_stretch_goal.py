def vowel_swapper(string):
    # ==============
    # Your code here
    #Sets counter variables
    aCount = 0
    eCount = 0
    iCount = 0
    oCount = 0
    uCount = 0
    word = list(string)                         #Converts the string to a list to allow the characters to be changed
    for x in range(0,len(word)):                
        #checks the character in index position x if it is a character to be changed
        #if character found the counter is increased
        #checks if the vowel counter is equal to 2 meaning it should be changed
        #changes the character in position x in the list to the new character
        #in the case of capital 'O' the different character change is an additional if statement
        
        if word[x] == 'a' or word[x] == 'A':    
            aCount = aCount+1                   
            if aCount == 2:                     
                word[x]='4'                      
                
        if word[x] == 'e' or word[x] == 'E':
            eCount = eCount+1
            if eCount == 2:
                word[x]='3'
        if word[x] == 'i' or word[x] == 'I':
            iCount = iCount+1
            if iCount == 2:
                word[x]='!'                  
        if word[x] == 'o' or word[x] == 'O':
            oCount = oCount+1
            if oCount == 2:
                if word[x] == 'O':
                    word[x] = '000'
                else:
                    word[x] = 'ooo'
            else:
                pass
                
        if word[x] == 'u' or word[x] == 'U':
            uCount = uCount+1
            if uCount == 2:
                uCount = 0
                word[x] = '|_|'
                
    string = "".join(word)    #the list is then joined back into a string 
    return string
    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
